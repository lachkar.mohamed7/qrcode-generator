package com.exaltit.application;


import com.exaltit.feign.GeneratorRestClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;


@SpringBootApplication
@ComponentScan({"com.exaltit"})
@EntityScan("com.exaltit")
@EnableJpaRepositories("com.exaltit")
@EnableSpringConfigured
@EnableAsync
@EnableFeignClients(basePackageClasses = GeneratorRestClient.class)
public class DemanderModuleApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemanderModuleApplication.class, args);
    }



}