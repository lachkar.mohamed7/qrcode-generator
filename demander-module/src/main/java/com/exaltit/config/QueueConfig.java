package com.exaltit.config;


import com.exaltit.model.ApplicantDTO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.concurrent.*;

@Configuration
public class QueueConfig {
    @Bean
    public ConcurrentLinkedQueue<List<ApplicantDTO>> requestQueueQRCodes() {
        return new ConcurrentLinkedQueue<>();
    }

    }
