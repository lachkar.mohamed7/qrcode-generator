package com.exaltit.entites;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Applicant {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long applicantId;
    private String name;
    private String lastName;
    private Date birthDate;
    private Boolean isVip;
    private Date demandeDate;
    private Date generationDate;
    @Column(length = 5000)
    private String pass;

}
