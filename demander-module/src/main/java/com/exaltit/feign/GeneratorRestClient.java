package com.exaltit.feign;

import com.exaltit.model.ApplicantDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(  name = "generator-module", url = "http://localhost:8082") //,
public interface GeneratorRestClient {

    @PostMapping("/api/generate/qr")
    List<ApplicantDTO> generatePasses(@RequestBody List<ApplicantDTO> applicantListIds);

}
