package com.exaltit.mappers;

import com.exaltit.entites.Applicant;
import com.exaltit.model.ApplicantDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ApplicantMapper {

    ApplicantMapper INSTANCE = Mappers.getMapper(ApplicantMapper.class);

    @Mapping(target = "applicantId", ignore = true)
    List<Applicant> applicantDTOToApplicant(List<ApplicantDTO> persons);
}
