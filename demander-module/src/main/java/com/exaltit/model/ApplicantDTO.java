package com.exaltit.model;

import lombok.Builder;

import java.util.Date;

@Builder
public record ApplicantDTO(
         String name,
         String lastName,
         Date birthDate,
         Boolean isVip,
         Date demandeDate,
         Date generationDate,
         String pass


         ) {
}
