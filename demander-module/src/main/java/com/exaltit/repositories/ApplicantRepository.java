package com.exaltit.repositories;

import com.exaltit.entites.Applicant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface ApplicantRepository extends JpaRepository<Applicant, Long> {

    @Query("SELECT ap FROM Applicant ap WHERE ap.name = :name AND ap.lastName = :lastName AND ap.birthDate = :birthDate")
    Optional<Applicant> findByNameLastNameAndBirthDate(@Param("name") String name, @Param("lastName") String lastName, @Param("birthDate") Date birthDate);
}
