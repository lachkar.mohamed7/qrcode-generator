package com.exaltit.rest;

import com.exaltit.services.AsyncService;
import com.exaltit.entites.Applicant;
import com.exaltit.model.ApplicantDTO;
import com.exaltit.services.ApplicantServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentLinkedQueue;

@RestController
@RequestMapping("/api/request")
public class ApplicantController {

    private final ApplicantServiceImpl applicantService;
    private final ConcurrentLinkedQueue<List<ApplicantDTO>> requestQueueQRCodes;
    private final AsyncService asyncService;

    public ApplicantController(ApplicantServiceImpl applicantService, ConcurrentLinkedQueue<List<ApplicantDTO>> requestQueueQRCodes, AsyncService asyncService) {
        this.applicantService = applicantService;
        this.requestQueueQRCodes = requestQueueQRCodes;
        this.asyncService = asyncService;
    }


    @Operation(summary = "Get list applicants", description = "Get All applicants ")
    @GetMapping("/applicants")
    public List<Applicant> getAllApplicant(){
        return applicantService.findAllApplicants();
    }


    @Operation(summary = "Launch generation", description = "Launch generation")
    @GetMapping("/qrcode")
    public CompletableFuture<List<ApplicantDTO>> generateInQueue() {

        // Generate Randoms
        List<ApplicantDTO> list = applicantService.generateRandomApplicants();

        //Apply priority
        asyncService.addPriority(list);

        // Add to queue
        asyncService.addToQueue(list);

        return  asyncService.processRequestsGenerating();
    }


    @Operation(summary = "Stop async requests", description = "Stop async requests")
    @GetMapping("/stop")
    public void stop(){
        asyncService.stopProcessing();
    }


    @Operation(summary = "Search applicant by params", description = "Search applicant by params ")
    @GetMapping("/applicant")
    public Optional<Applicant> getApplicantById(@RequestParam String name, @RequestParam  String lastName, @RequestParam String birthDate){
        return applicantService.getApplicantByNameAndLastNameAndBirthDate(name, lastName, birthDate);
    }

}
