package com.exaltit.services;

import com.exaltit.model.ApplicantDTO;
import java.util.List;

/*


    Generate Random Applicants : For now 10 applicants will be generated
    Call Micro Service Generator to generate QR code in async way

 */
public interface ApplicantService {

    List<ApplicantDTO> generatePassWithRandomQueue(List<ApplicantDTO> list);
    List<ApplicantDTO> generateRandomApplicants();

}
