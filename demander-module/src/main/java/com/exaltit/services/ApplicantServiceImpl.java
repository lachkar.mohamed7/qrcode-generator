package com.exaltit.services;

import com.exaltit.entites.Applicant;
import com.exaltit.mappers.ApplicantMapper;
import com.exaltit.model.ApplicantDTO;
import com.exaltit.repositories.ApplicantRepository;
import com.exaltit.feign.GeneratorRestClient;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class ApplicantServiceImpl implements ApplicantService {

    private final ApplicantRepository applicantRepository;
    private final GeneratorRestClient generatorRestClient;
    @Value("${number.applicants}")
    private String numberOfApplicants;


    public ApplicantServiceImpl(ApplicantRepository applicantRepository, GeneratorRestClient generatorRestClient) {
        this.applicantRepository = applicantRepository;
        this.generatorRestClient = generatorRestClient;

    }
    public List<Applicant> findAllApplicants() {
        return applicantRepository.findAll();
    }


    public List<ApplicantDTO> generatePassWithRandomQueue(List<ApplicantDTO> listApplicants) {

        // call generator module
        List<ApplicantDTO> listApplicantsWithPass = generatorRestClient.generatePasses(listApplicants);

        // save applicants in h2 db
        applicantRepository.saveAll(ApplicantMapper.INSTANCE.applicantDTOToApplicant(listApplicantsWithPass));

        return listApplicantsWithPass;
    }

    public List<ApplicantDTO> generateRandomApplicants() {
        List<ApplicantDTO> listApplicants = new ArrayList<>();
        for (int i = 0; i < Integer.parseInt(numberOfApplicants); i++) {
            ApplicantDTO applicant = ApplicantDTO.builder()
                    .name(RandomStringUtils.random(7, true, false))
                    .lastName(RandomStringUtils.random(7, true, false))
                    .isVip(Math.random() < 0.5)
                    .demandeDate(new Date())
                    .birthDate(generateRandomDate())
                    .build();
            listApplicants.add(applicant);
        }
        return listApplicants;

    }

    private Date generateRandomDate() {
        int year = ThreadLocalRandom.current().nextInt(1900, 2020);
        int month = ThreadLocalRandom.current().nextInt(1, 12);
        int maxDay = LocalDate.of(year, month, 1).lengthOfMonth();
        int day = ThreadLocalRandom.current().nextInt(1, maxDay + 1);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month - 1);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        return calendar.getTime();
    }


    public Optional<Applicant> getApplicantByNameAndLastNameAndBirthDate(String name, String lastName, String birthDate) {

        Instant instant = Instant.parse(birthDate);
        Date date = Date.from(instant);

        return applicantRepository.findByNameLastNameAndBirthDate(name, lastName, date);

    }

}
