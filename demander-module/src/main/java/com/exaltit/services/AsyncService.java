package com.exaltit.services;

import com.exaltit.model.ApplicantDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentLinkedQueue;

@Service
public class AsyncService {
    private final ConcurrentLinkedQueue<List<ApplicantDTO>> requestQueueQRCodes;
    private final ApplicantService applicantService;
    Logger logger = LoggerFactory.getLogger(AsyncService.class);

    private volatile boolean processing;

    public AsyncService(ConcurrentLinkedQueue<List<ApplicantDTO>> requestQueueQRCodes, ApplicantService applicantService) {
        this.requestQueueQRCodes = requestQueueQRCodes;
        this.applicantService = applicantService;
        this.processing = true;
    }

    @Async
    public CompletableFuture<List<ApplicantDTO>> processRequestsGenerating() {
        while (true) {
            List<ApplicantDTO> request = requestQueueQRCodes.poll();

            logger.info("Processing request : " );
            List<ApplicantDTO> response = applicantService.generatePassWithRandomQueue(request);
            logger.info("Request processed : " );

            return CompletableFuture.completedFuture(response);
        }
    }

    public void stopProcessing() {
        this.processing = false;
    }

    public void addToQueue(List<ApplicantDTO> listApplicants){
        requestQueueQRCodes.offer(listApplicants);
    }

    public void addPriority(List<ApplicantDTO> listApplicants) {
         listApplicants.sort(Comparator.comparing(ApplicantDTO::isVip).reversed());
    }

}
