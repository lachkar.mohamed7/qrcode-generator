package com.exaltit.application;

import com.exaltit.entites.Applicant;
import com.exaltit.mappers.ApplicantMapper;
import com.exaltit.model.ApplicantDTO;
import com.exaltit.repositories.ApplicantRepository;
import com.exaltit.feign.GeneratorRestClient;
import com.exaltit.services.ApplicantServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
class ApplicantServiceTest {

    @InjectMocks
    ApplicantServiceImpl applicantServiceImpl;

    @Mock
    ApplicantRepository applicantRepository;

    @Mock
    GeneratorRestClient generatorRestClient;


    @BeforeEach
    void init() {
        applicantServiceImpl = new ApplicantServiceImpl(applicantRepository, generatorRestClient);
    }


    @DisplayName("Test findAllApplicants")
    @Test
    void shouldFindAllApplicants() {

        List<ApplicantDTO> actualList = new ArrayList<>();

        actualList.add(ApplicantDTO.builder()
                .name("user1")
                .lastName("lastname1")
                .demandeDate(new Date())
                .birthDate(new Date())
                .build());
        actualList.add(ApplicantDTO.builder()
                .name("user1")
                .lastName("lastname1")
                .demandeDate(new Date())
                .birthDate(new Date())
                .build());

        Mockito.when(applicantRepository.findAll()).thenReturn(ApplicantMapper.INSTANCE.applicantDTOToApplicant(actualList));
        List<Applicant> result = applicantServiceImpl.findAllApplicants();

        assertNotNull(actualList);
        assertEquals(2, result.size());
        assertEquals("user1", result.get(0).getName());

    }

    @DisplayName("Test generate Pass With Random Queue")
    @Test
    void shouldGeneratePassWithRandom() {

        List<ApplicantDTO> actualList = new ArrayList<>();
        actualList.add(ApplicantDTO.builder()
                .name("user1")
                .lastName("lastname1")
                .demandeDate(new Date())
                .birthDate(new Date())
                .build());

        List<ApplicantDTO> actualListAfterGen = new ArrayList<>();
        actualListAfterGen.add(ApplicantDTO.builder()
                .name("user1")
                .lastName("lastname1")
                .demandeDate(new Date())
                .birthDate(new Date())
                .pass("qrcode")
                .build());

        List<Applicant> applicants = ApplicantMapper.INSTANCE.applicantDTOToApplicant(actualListAfterGen);
        Mockito.when(generatorRestClient.generatePasses(actualList)).thenReturn(actualListAfterGen);
        Mockito.when(applicantRepository.saveAll(applicants)).thenReturn(applicants);

        List<ApplicantDTO> result = applicantServiceImpl.generatePassWithRandomQueue(actualList);

        assertNotNull(actualListAfterGen);
        assertNotNull(actualList);
        assertEquals(1, result.size());
        assertNotNull(result.get(0));
        assertEquals("user1", result.get(0).name());
        assertEquals("qrcode", result.get(0).pass());

    }



}
