package com.exaltit.application;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.exaltit"})
@EntityScan("com.exaltit")
public class GeneratorModuleApplication {
    public static void main(String[] args) {
        SpringApplication.run(GeneratorModuleApplication.class, args);
    }
}
