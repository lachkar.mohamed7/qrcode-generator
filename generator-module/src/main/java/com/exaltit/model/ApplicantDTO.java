package com.exaltit.model;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Builder
@Data
public class ApplicantDTO {

    private String name;
    private String lastName;
    private Date birthDate;
    private Boolean isVip;
    private Date demandeDate;
    private Date generationDate;
    private String pass;
}
