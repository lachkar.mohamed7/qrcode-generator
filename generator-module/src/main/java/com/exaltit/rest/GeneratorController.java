package com.exaltit.rest;


import com.exaltit.model.ApplicantDTO;
import com.exaltit.service.GeneratorQrCodeServiceImpl;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/generate")
public class GeneratorController {

    private final GeneratorQrCodeServiceImpl generatorService;

    GeneratorController(GeneratorQrCodeServiceImpl generatorService) {
        this.generatorService = generatorService;
    }

    @PostMapping("/qr")
    public List<ApplicantDTO> launchGeneration(@RequestBody List<ApplicantDTO> applicantsList){
        return generatorService.generate(applicantsList);
    }

}
