package com.exaltit.service;

import com.exaltit.model.ApplicantDTO;
import java.util.List;

/*

    Methode to generate QR Code of a list of applicants
    Return a list of Applicants with pass according to every applicant
    Les information d'un pass : Prénom, Nom, DateNaissance, Status_VIP, DateHeureDeLaDemande, DateHeureDeGénération

 */
public interface GeneratorQrCodeService {

    List<ApplicantDTO> generate(List<ApplicantDTO> applicantsList);
}
