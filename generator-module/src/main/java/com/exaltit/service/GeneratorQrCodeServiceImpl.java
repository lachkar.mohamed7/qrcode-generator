package com.exaltit.service;

import com.exaltit.model.ApplicantDTO;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageConfig;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import org.springframework.stereotype.Service;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class GeneratorQrCodeServiceImpl implements GeneratorQrCodeService {

    public List<ApplicantDTO> generate(List<ApplicantDTO> applicantsList) {
        // filtrer les demandeurs : toute les informations doivent exister
        List<ApplicantDTO> applicantListFiltered = filterListApplicant(applicantsList);
        return applicantListFiltered.stream()
                .map(applicant -> {
                    Date date = new Date();
                    applicant.setGenerationDate(date);
                    applicant.setPass(getQrCodeFromImage(
                           new StringBuilder(applicant.getName())
                                   .append(applicant.getLastName())
                                   .append(applicant.getBirthDate())
                                   .append(applicant.getIsVip())
                                   .append(applicant.getDemandeDate().toString())
                                   .append(date)
                            ));
                    return applicant;
                }).toList();
    }

    public List<ApplicantDTO> filterListApplicant(List<ApplicantDTO> applicantsList) {
        return
                applicantsList.stream()
                        .filter(Objects::nonNull)
                        .filter(applicantDTO ->
                                applicantDTO.getName() != null
                                && applicantDTO.getBirthDate() != null
                                && applicantDTO.getDemandeDate() != null
                                && applicantDTO.getLastName() != null)
                        .toList();
    }


    public String getQrCodeFromImage(StringBuilder applicantInfo) {
        byte[] image = new byte[0];
        try {
            image = getQRCode(applicantInfo);

        } catch (WriterException | IOException e) {
            e.getMessage();
        }
        // Convert Byte Array into Base64 Encode String
        return Base64.getEncoder().encodeToString(image);
    }

    private static byte[] getQRCode(StringBuilder text) throws WriterException, IOException {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = qrCodeWriter.encode(text.toString(), BarcodeFormat.QR_CODE, 150, 150);

        ByteArrayOutputStream pngOutputStream = new ByteArrayOutputStream();
        MatrixToImageConfig con = new MatrixToImageConfig( 0xFF000002 , 0xFFFFC041 ) ;

        MatrixToImageWriter.writeToStream(bitMatrix, "PNG", pngOutputStream,con);
        return pngOutputStream.toByteArray();
    }

}
