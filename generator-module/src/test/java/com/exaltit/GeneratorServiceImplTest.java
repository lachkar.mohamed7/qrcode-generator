package com.exaltit;

import com.exaltit.model.ApplicantDTO;
import com.exaltit.service.GeneratorQrCodeServiceImpl;
import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class GeneratorServiceImplTest {

    GeneratorQrCodeServiceImpl generatorQrCodeService;

    @BeforeEach
    void init(){
        generatorQrCodeService = new GeneratorQrCodeServiceImpl();
    }

    @DisplayName("Test generate qr code from a list")
    @Test
    void shouldTestGenerate(){
        List<ApplicantDTO> actualList = new ArrayList<>();

        actualList.add( ApplicantDTO.builder()
                .name("user1")
                .lastName("lastname1")
                .demandeDate(new Date())
                .birthDate(new Date())
                .build());
        actualList.add( ApplicantDTO.builder()
                .name("user2")
                .lastName("lastname1")
                .demandeDate(new Date())
                .birthDate(new Date())
                .build());

        List<ApplicantDTO> listExpected = generatorQrCodeService.generate(actualList);
        assertNotNull(listExpected);
        assertEquals(2,listExpected.size());
        assertNotNull(listExpected.get(1).getPass());
        assertNotNull(listExpected.get(1).getGenerationDate());

    }

    @DisplayName("Test generate String qr code")
    @Test
    void shouldGetQrCode(){
        StringBuilder input = new StringBuilder("mWKpkQE");
        String expected ="iVBORw0KGgoAAAANSUhEUgAAAJYAAACWCAIAAACzY+a1AAAEkElEQVR4Xu2RS45ERQwER9yMm3Mz2M3CtqLwrxujlGLVmbbrdfz8/def4jQ//idxCyk8jxSeRwrPI4XnkcLzSOF5pPA8UngeKTyPFJ5HCs8jheeRwvNI4Xmk8DxSeB4pPI8UnkcKzyOF5xlQ+PPzx2fwp+EZnHbKg/ivKCCF6fIg/isKSGG6PIj/igJSmC4P4r+igBSmy4P4rygwr9AXyqQ2+z8I8OOwyhfKbGyWwoBUOcXGZikMSJVTbGyWwoBUOcXGZikMSJVTbGxeV2hShmc53StzyvCsSWtI4bvMKcOzJq0hhe8ypwzPmrSGFL7LnDI8a9IaUvguc8rwrElrHFaYKnOaKpuU4Vl/uoAUBnDZpAzP+tMFpDCAyyZleNafLiCFAVw2KcOz/nQBKQzgskkZnvWnC/x/FKZmB1OGZ01aQwq7KcOzJq0hhd2U4VmT1pDCbsrwrElrSGE3ZXjWpDXWFXb41mZOO2xslsJgM6cdNjZLYbCZ0w4bm6Uw2Mxph43NUhhs5rTDxuZ5hXvw3b10D3O3hhS+0z3M3RpS+E73MHdrSOE73cPcrSGF73QPc7fGgMJv4f8R+Hd8Acq3kMKgfAspDMq3kMKgfAspDMq3GFDo/xGgMztI6hn+k4HObA0pfD/DfzLQma0hhe9n+E8GOrM1pPD9DP/JQGe2hhS+n+E/GejM1hhQaOBv4DSFWXVi8wZSGOC3TW3eQAoD/LapzRtIYYDfNrV5AykM8NumNm8wr9Dg/4J//3f8R8o8y6t8Aco1pPBd5lle5QtQriGF7zLP8ipfgHINKXyXeZZX+QKUa0jhu8yzvMoXoFxjXSHjv+rffyGXOWU6s59HCgM6s59HCgM6s59HCgM6s59HCgM6s59nQKH5YIZnOU2VTZoq+1tAata/pI8UBmV/C0jN+pf0kcKg7G8BqVn/kj5SGJT9LSA161/SRwqDsr8FpGb9S/oMKPwWqX+Hy4NpCrOqhhQOpynMqhpSOJymMKtqSOFwmsKsqiGFw2kKs6rGgEL/siX4LqedcorUZlOuIYXvcorUZlOuIYXvcorUZlOuIYXvcorUZlOuIYXvcorUZlOuMa/QF8rw5lSawr9kio1DUhjgXzLFxiEpDPAvmWLjkBQG+JdMsXFICgP8S6bYOLSu0P9BAM+m0lTZpN8q15DCIP1WuYYUBum3yjWkMEi/Va4hhUH6rXKNwwo7ZSa1isucjiCFAalVXOZ0BCkMSK3iMqcjSGFAahWXOR1BCgNSq7jM6QiHFe6lXObZTlpDCoOUyzzbSWtIYZBymWc7aQ0pDFIu82wnrSGFQcplnu2kNdYVduDNnHbgzZx2yjWkMIA3c9op15DCAN7MaadcQwoDeDOnnXINKQzgzZx2yjXmFe7Bd/3DAJ7lNIVZNbj5FykMZjlNYVYNbv5FCoNZTlOYVYObf5HCYJbTFGbV4OZfpDCY5TSFWTW4+ZcBheK7SOF5pPA8UngeKTyPFJ5HCs8jheeRwvNI4Xmk8DxSeB4pPI8UnkcKzyOF55HC80jheaTwPFJ4Hik8jxSeRwrP8w9T8wLEPb7w8wAAAABJRU5ErkJggg==";
        String qr = generatorQrCodeService.getQrCodeFromImage(input);
        assertEquals(qr.length(), expected.length());
        assertEquals(qr, expected);
    }

    @DisplayName("Test filter list applicants")
    @Test
    void shouldFilterList(){
        List<ApplicantDTO> actualList = new ArrayList<>();

        actualList.add( ApplicantDTO.builder()
                        .name("user1")
                        .lastName("lastname1")
                        .demandeDate(new Date())
                        .birthDate(new Date())
                        .build());
        actualList.add(null);

        List<ApplicantDTO> listExpected = generatorQrCodeService.filterListApplicant(actualList);
        assertNotNull(listExpected);
        assertEquals(1,listExpected.size());
    }

}
